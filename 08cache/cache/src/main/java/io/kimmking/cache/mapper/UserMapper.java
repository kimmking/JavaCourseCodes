package io.kimmking.cache.mapper;

import io.kimmking.cache.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface UserMapper {

    @Select("select * from user where id = #{id}")
    User find(int id);

    @Select("select * from user")
    List<User> list();

    @Select("select * from user where id = #{id} and name=#{name}")
    User find2(int id, String name);

}
