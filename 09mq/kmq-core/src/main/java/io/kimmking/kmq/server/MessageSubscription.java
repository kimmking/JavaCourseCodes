package io.kimmking.kmq.server;

import lombok.Data;

/**
 * Description for this class.
 *
 * @Author : kimmking(kimmking@apache.org)
 * @create 2024/6/30 下午6:49
 */

@Data
public class MessageSubscription {
    private String clientId;
    private String topic;
    private Long offset;
}
