package io.kimmking.kmq.core;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Description for this class.
 *
 * @Author : kimmking(kimmking@apache.org)
 * @create 2024/6/30 下午6:12
 */

@Data
@AllArgsConstructor
public class Result {

    private int code;
    private KmqMessage<?> message;

    public static Result ok() {
        return msg("OK");
    }

    public static Result msg(String msg) {
        return new Result(1, new KmqMessage<>(msg));
    }
}
