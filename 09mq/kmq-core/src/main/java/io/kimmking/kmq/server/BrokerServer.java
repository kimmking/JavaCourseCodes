package io.kimmking.kmq.server;

import io.kimmking.kmq.core.KmqMessage;
import io.kimmking.kmq.core.Result;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Description for this class.
 *
 * @Author : kimmking(kimmking@apache.org)
 * @create 2024/6/30 下午6:01
 */

@Controller("kmq")
@RequestMapping("/kmq")
public class BrokerServer {

    @RequestMapping("/recv")
    public Result recv(String clientId) {
        return Result.msg("hello,"+clientId);
    }

    @RequestMapping("/send")
    public Result send(String clientId, KmqMessage<?> message) {
        return Result.ok();
    }

    @RequestMapping("/ack")
    public Result ack(String clientId, Long offset) {
        return Result.msg("OK");
    }


}
