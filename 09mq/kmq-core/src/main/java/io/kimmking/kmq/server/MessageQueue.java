package io.kimmking.kmq.server;

import io.kimmking.kmq.core.KmqMessage;

/**
 * Description for this class.
 *
 * @Author : kimmking(kimmking@apache.org)
 * @create 2024/6/30 下午6:44
 */
public class MessageQueue {

    private String topic;
    private int index = 0;
    KmqMessage<?>[] queue = new KmqMessage[1024 * 10];

    public MessageQueue(String topic) {
        this.topic = topic;
    }

    public void put(KmqMessage<?> message) {
        queue[index++] = message;
    }

}
