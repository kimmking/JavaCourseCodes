package io.kimmking.ss.ssdemo1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
    
    @Autowired
    private UserMapper mapper;
    
    @GetMapping("/user/find")
    public User find(){
        return mapper.selectById(100);
    }
}
