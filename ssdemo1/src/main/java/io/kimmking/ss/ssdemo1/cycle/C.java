package io.kimmking.ss.ssdemo1.cycle;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("c")
@Order(3)
public class C {
    
    @Autowired
    private List<I> list; 
    
    public C() { //List<I> list){
        //this.list = list;
    }
}
