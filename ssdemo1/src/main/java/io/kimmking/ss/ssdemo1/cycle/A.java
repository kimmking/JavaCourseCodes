package io.kimmking.ss.ssdemo1.cycle;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component("a")
@Order(1)
public class A implements I {
    
    //@Lazy
    @Autowired
    private Map<String,C> maps;
    
}
