package io.kimmking.ss.ssdemo1.cycle;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component("b")
@Order(2)
public class B {
    
    @Autowired
    private Map<String,C> maps;
    
    
}
