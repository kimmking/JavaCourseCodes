package io.kimmking.ss.ssdemo1;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface UserMapper {
    
    @Select("select id,name,created from user where id=#{id}")
    User selectById(@Param("id") Integer id);
    
}
